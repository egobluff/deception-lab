#!/bin/bash -i
#script for starting container
cd /home/itadmin/opencanary
#build docker
docker build -t opencanary -f Dockerfile.stable .
#check images
docker images
#future adventures in ipvlan networking
#docker network  create -d ipvlan --subnet=192.168.0.0/24 --gateway=192.168.0.1 -o ipvlan_mode=l2 -o parent=eth0 ipcanarynet
docker network  create -d ipvlan --subnet=192.168.101.0/24 --gateway=192.168.101.111 -o ipvlan_mode=l2 -o parent=enp0s3 ipcanarynet
#sudo docker network  create  -d ipvlan \
#	--subnet=192.168.0.0/24 \
#	--gateway=192.168.0.1  \
#	 -o ipvlan_mode=l2 \
#	 -o parent=eth0 ipcanarynet

touch /home/itadmin/opencanary/foobar
ping -c 20 127.0.0.1
echo "fin"
rm /home/itadmin/opencanary/foobar

